Rails.application.routes.draw do
  root to: 'visitors#index'

  get 'next', to: 'visitors#random_corgi', as: 'next_corgi'
end
