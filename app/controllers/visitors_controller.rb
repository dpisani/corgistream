class VisitorsController < ApplicationController

  def index
    @corgi = ImgurService.get_random_corgi
  end

  def random_corgi
    render json: ImgurService.get_random_corgi
  end
end
