require 'net/http'
require 'net/https'

class ImgurService
  class << self

    def get_random_corgi
      url = URI.parse("https://api.imgur.com/3/gallery/r/corgi/time/#{rand(40)}")
      request = Net::HTTP::Get.new(url.to_s)
      request.add_field("Authorization", "Client-ID #{Rails.application.secrets.imgur_client_id}")
      res = Net::HTTP.start(url.host, url.port, :use_ssl => url.scheme == 'https') do |http|
        http.use_ssl = true
        http.request(request)
      end
      json = JSON.parse(res.body)
      return nil unless res.code == '200'
      json["data"].sample
    end
  end
end
