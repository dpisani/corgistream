window.readyForCorgi = false;

function setCorgi(image, title, description) {
  window.readyForCorgi = false;
  $('<img/>').attr('src', image).load(function() {
    $(this).remove(); // prevent memory leaks as @benweet suggested
    $('#big-corgi').css('background-image', 'url('+ image +')');
    $("#corgi-title").text(title);
    if (!description)
      description = "";
    $("#corgi-description").text(description);
    $("#next-button").text("More Corgi!");
    window.readyForCorgi = true;
  });
}

function nextCorgi(getUrl) {
  if (window.readyForCorgi) {
    $("#next-button").text("Loading...");
    $.get(getUrl, function(data) {
      if (data)
      {
        setCorgi(data.link, data.title, data.description);
      }
      else {
        console.log("oh no!");
        $("#next-button").text("That didnt't work :( Try again");
      }
    });
  }
}

function toggleAuto(getUrl) {
  if (window.corgiTimer) {
    clearInterval(window.corgiTimer);
    window.corgiTimer = null;
    $("#auto-button").text("Auto Corg!");
  }
  else {
    $("#auto-button").text("Stop");
    window.corgiTimer = setInterval(function() {
      nextCorgi(getUrl);
    }, 2000);
  }
}
