Corgi Stream
================

Do you want an endless stream of corgi pictures?

What sort of question is that? Of course you do.

What *IS* this?
---------------
Its a web app that takes corgi pics of imgur and delivers them to you for your convenience